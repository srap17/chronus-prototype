# CHRONUS UI/UX Prototype

To run this prototype, clone this git and follow instructions below:

1. Install Vite: 
    ```
    $ npm install vite@2.0.5
    ```
2. Install ANT Design for Vue and its dependency:  
    ```
    $ npm install ant-design-vue@2.0.1 @ant-design/icons-vue
    ```
3. Install Apexchart for Vue:
    ```
    $ npm install apexchart vue3-apexchart
    ```
4. Run package in dev:
    ```
    $ npm run dev
    ```
