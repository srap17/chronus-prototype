import Vue from 'vue';
import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import routes from 'voie-pages';
import App from './App.vue';
import './main.css';
import VueApexCharts from "vue3-apexcharts";
import Antd from 'ant-design-vue';



// import { icon } from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
// import * as AntIcon from '@ant-design/icons-vue';

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// Vue.component(Filter.name)

const app = createApp(App)
app.use(router);
app.use(VueApexCharts);
app.use(Antd)
// app.use(Icon)
// app.use(AntIcon)
app.mount('#app');
